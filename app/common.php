<?php

use PHPMailer\PHPMailer\PHPMailer;
/**
 * @param $toemial 用户注册邮箱
 * @param string $title  激活邮件标题
 * @param $account 用户账号
 * @param $url 激活地址
 */
function sendEmail($toEmial,$title,$content){
    $mail = new PHPMailer(true);
    $SmtpInfo = \think\Db::name("system")->where("config","smtp")->find();
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // 调试模式 0 ，1 ，2
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.sina.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $SmtpInfo["value"];                 // SMTP username
        $mail->Password = $SmtpInfo["extend"];                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $SmtpInfo["intro"];                                    // TCP port to connect to 25 or 587
        $mail->CharSet = 'UTF-8';//邮件编码的设置
        //Recipients
        $mail->setFrom('mmteen@sina.com', 'TnCMS');
        $mail->addAddress($toEmial);               // Name is optional
        $mail->addReplyTo('mmteen@sina.com', 'TnCMS');

        //Content
        $mail->isHTML(true);                              // Set email format to HTML
        $mail->Subject = $title;
        $mail->Body    = $content;

        $mail->send();
        return ["code" => 200 ,"msg" => "邮件发送成功"];
    } catch (Exception $e) {
        return ["code" => 200 ,"msg" =>$mail->ErrorInfo];
    }
}

function isMobile() {
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return true;

    //此条摘自TPM智能切换模板引擎，适合TPM开发
    if(isset ($_SERVER['HTTP_CLIENT']) &&'PhoneClient'==$_SERVER['HTTP_CLIENT'])
        return true;
    //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
        //找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
    //判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array(
            'nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile'
        );
        //从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    //协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
    return false;
}

function curlData($url, $data=null,$method='GET', $https=true)
{
    // 创建一个新cURL资源
    $ch = curl_init();
    // 设置URL和相应的选项
    curl_setopt($ch, CURLOPT_URL, $url);
    //要访问的网站 //启用时会将头文件的信息作为数据流输出。
    curl_setopt($ch, CURLOPT_HEADER, false);
    //将curl_exec()获取的信息以字符串返回，而不是直接输出。
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if($https){

        //FALSE 禁止 cURL 验证对等证书（peer's certificate）。
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        //验证主机 }
        if($method == 'POST'){
            curl_setopt($ch, CURLOPT_POST, true);

            //发送 POST 请求  //全部数据使用HTTP协议中的 "POST" 操作来发送。
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        // 抓取URL并把它传递给浏览器
        $content = curl_exec($ch);
        //关闭cURL资源，并且释放系统资源
        curl_close($ch);

        return json_decode($content,true);

    }
}

function xmlToArray($xml)
{
    //禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $values;
}

function isEmail($email){
    $pattern="/([a-z0-9]*[-_.]?[a-z0-9]+)*@([a-z0-9]*[-_]?[a-z0-9]+)+[.][a-z]{2,3}([.][a-z]{2})?/i";
    if(preg_match($pattern,$email)){
        return true;
    } else{
       return false;
    }
}
