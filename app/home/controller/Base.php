<?php
namespace app\home\controller;

use think\facade\Db;
use app\common\controller\Auth;
use think\App;
use think\exception\ValidateException;
use think\exception\HttpResponseException;
use think\facade\Request;
use think\facade\View;
use think\Validate;
use app\common\model\Post;
abstract class Base
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];


    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {

        $this->app     = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {

        $this->auth();
        $data=Db::name('nav')->order("order desc")->select();
        $nav_main = array();
        foreach ($data as $item){
            $nav_main[$item["parent_id"]][] = $item;

        }

        $siteInfo= Db::name("system")->where("config","siteInfo")->find();
        $tag= Db::name("category")->order( 'cate_id desc')->select();;//热门标签
        $hot_list = Post::order("read_count desc")->paginate(6);//热门文章
        $link = Db::name("link")->where("type",0)->select();

        if(session("UID")){
            $userInfo = Db::name("user")->where("uid",session("UID"))->find();
        }else{
            $userInfo = null;
        }
        //自定义JS代码
        $statistics = Db::name("system")->where("config","statistics")->value("value");

        //底部链接

        $cateLink1 = $this->getCateLink(1);
        $cateLink2 = $this->getCateLink(2);
        $cateLink3 = $this->getCateLink(3);
        $cateLink4 = $this->getCateLink(4);

        $cateWhere1["cate_id"] = 2;
        $cateWhere1["status"] = 0;
        $commonRightNews1 = $this->getCatePost($cateWhere1,5);

        $cateWhere2["cate_id"] = 3;
        $cateWhere2["status"] = 0;
        $commonRightNews2 = $this->getCatePost($cateWhere2,5);

        $newList = Post::where("status",0)->order("id desc")->limit(5)->select();
        View::assign([
            'nav'=>$nav_main,
            'siteInfo'=>json_decode($siteInfo["value"],true),
            'link'=>$link,
            'userInfo'=>$userInfo,
            'tag'=>$tag ,
            'hot_list' => $hot_list,
            'statistics' => $statistics,
            'cateLink1' => $cateLink1,
            'cateLink2' => $cateLink2,
            'cateLink3' => $cateLink3,
            'cateLink4' => $cateLink4,
            'commonRightNews1' => $commonRightNews1,
            'commonRightNews2' => $commonRightNews2,
            'newList' => $newList,
        ]);
    }
    public function getCateLink($type)
    {
        $Link = Db::name("link")->where("type",$type)->select();
        return $Link;
    }

    public function getCatePost($where,$limit)
    {
        $commonRightNews = Post::where($where)->order("id desc")->limit($limit)->select();
        $commonRightNews[0]["cate_name"] = Db::name("category")->where("cate_id",$where["cate_id"])->value("cate_name");

        return $commonRightNews;
    }

    public function auth()
    {
        $auth = new Auth();
        $Route = 'home/'.Request::controller(true) . '/' . Request::action();

        //权限白名单
        $filter = [
            'home/index/index',
            'home/post/newList',
            'home/post/detail',
            'home/post/category',
            'home/member/login',
            'home/member/register',
            'home/member/wxLogin',
            'home/member/logout'
        ];

        if(!$auth->check($Route, session('UID'),$filter)){
            $this->error("没有访问权限",'/home/member/login');
        }
    }
    /**
     * 操作错误跳转
     * @param  mixed   $msg 提示信息
     * @param  string  $url 跳转的URL地址
     * @param  mixed   $data 返回的数据
     * @param  integer $wait 跳转等待时间
     * @param  array   $header 发送的Header信息
     * @return void
     */
    protected function error($msg = '', string $url = null, $data = '', int $wait = 3, array $header = []): Response
    {
        if (is_null($url)) {
            $url = request()->isAjax() ? '' : 'javascript:history.back(-1);';
        } elseif ($url) {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : app('route')->buildUrl($url)->__toString();
        }

        $result = [
            'code' => 0,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];

        $type = (request()->isJson() || request()->isAjax()) ? 'json' : 'html';
        if ($type == 'html'){
            $response = view("/error", $result);
        } else if ($type == 'json') {
            $response = json($result);
        }
        throw new HttpResponseException($response);
    }
    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                list($validate, $scene) = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }
}