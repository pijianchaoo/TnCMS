<?php

use think\facade\Db;

$theme = Db::name("system")->where("config","theme")->find();

if (request()->isMobile()){
    $themeTitle = $theme["extend"];
}else{
    $themeTitle = $theme["value"];
}

return [
    // 模板路径
    'view_path'    => 'themes/' .$themeTitle .'/',

];
