<?php
namespace app\api\controller;

use app\common\model\Post as P;
use think\facade\Db;

class Article
{
    //轮播图文章
    public function carousel()
    {
        $carouselConfig = Db::name("system")->where("config","carousel")->value("value");
        $carouselArticle = Db::name("post")->field("id,title,cover_image")->where("status",0)->where("id","in", $carouselConfig)->select();
        return json($carouselArticle);
    }
    //置顶文章
    public function topArticle()
    {
        $configTopArticle = Db::name("system")->where("config","topArticle")->value("value");
        $topArticle = P::withJoin(['userInfo'	=>	['username','uid'],'cate' =>['cate_name']])->where("status",0)->where("id","in",$configTopArticle)->limit(4)->select();
        return json($topArticle);
    }
    //最新文章
    public function newList()
    {
        $list = Db::name('post')->withoutField("content")->order("id desc")->where("status",0)->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
            if (strpos($item["cover_image"],"http") === false){
                if (empty($item["cover_image"])){
                    $item["cover_image"] = request()->domain() ."/uploads/default/default_cover.jpg";
                }else{
                    $item["cover_image"] = request()->domain() .$item["cover_image"];
                }
            }
            return $item;
        });
        return json($list);
    }

    //文章详情
    public function detail()
    {
        $id = input('id');
        Db::name('post')->where('id',$id)->inc('read_count')->update();
        $detail = P::withJoin(['userInfo'	=>	['username','uid'],'cate' =>['cate_name']])->where("tn_post.status",0)->where('id',$id)->order("id desc")->find();
        return json($detail);
    }

    //我的文章
    public function myPost()
    {
        $where["uid"] = input("UID");
        $list = P::with(["userInfo","cate"])->where($where)->order("id desc")->paginate(10);
        return $list;
    }
    //分类文章
    public function cateArticle()
    {
        $id = input('cateId');
        $newList = Db::name('post')->order("id desc")->where("cate_id",$id)->where("status",0)->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
            if (strpos($item["cover_image"],"http") === false){
                if (empty($item["cover_image"])){
                    $item["cover_image"] = request()->domain() ."/uploads/default/default_cover.jpg";
                }else{
                    $item["cover_image"] = request()->domain() .$item["cover_image"];
                }
            }
            return $item;
        });
        return json($newList);
    }
 
    //分类列表
    public function categoryList()
    {
        $list = Db::name("category")->select()->toArray();

        foreach ($list as $key=> $item){
            if (empty($item["cate_cover_image"])){
                $list[$key]["cate_cover_image"] = request()->domain() ."/static/images/api/default_cate_cover.jpg";
            }
        }
        return json($list);
    }

}