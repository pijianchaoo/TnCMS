<?php


namespace app\admin\controller;


use think\facade\Db;
use think\facade\View;

class Theme
{

    public function index()
    {

        View::assign("list",$this->getThemeList());
        return View::fetch("/themes");
    }

    public function getThemeList()
    {
        $list= scandir("themes");

        $dirList = array();
        foreach ($list as $item){
            if ($item == "." || $item == ".."){continue;}
            array_push($dirList,$item);
        }
        $themeList = array();
        foreach ($dirList as $item){
            $filePath = "themes/" .$item ."/info.php";
            $isExists = file_exists($filePath);
            if ($isExists){
                require_once($filePath);
                array_push($themeList,$info);
            }
        }
        return $themeList;
    }

    public function updateTheme()
    {
        $type = input("type");
        $title = input("title");

        if ($type == 1){
            $data["value"] = $title;
        }

        if ($type == 2){
            $data["extend"] = $title;
        }

        $update = Db::name("system")->where("config","theme")->update($data);

        if ($update){
            return json(["code" => 1 ,"msg" => "设置成功"]);
        }
        return json(["code" => 0 ,"msg" => "设置失败"]);
    }
}