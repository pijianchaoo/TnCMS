<?php

namespace app\admin\controller;

use  app\common\validate\Post as postValidate;
use app\home\controller\Wechat;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\Request;
use think\facade\View;
use app\common\model\Post as P;
use  app\common\model\Category;
class Post extends Base
{

    public function postList()
    {

        $cateId = input("cateId");
        $status = input("status");
        $keyword = input("keyword");

        $where = array();
        if (!empty($cateId) && empty($status) && empty($keyword)){
            $where["cate_id"] = $cateId;
        }
        if (empty($cateId) && $status!= null && empty($keyword)){
            $where["status"] = $status;
        }
        if (empty($cateId) && empty($status) && !empty($keyword)){
            $list = P::with(["userInfo","cate"])->where("title","like","%$keyword%")->order("id desc")->paginate(10);
        }else{
            $list = P::with(["userInfo","cate"])->where($where)->order("id desc")->paginate(10);
        }
        $cate = Db::name("category")->select();
        View::assign("list",$list);
        View::assign("cate",$cate);
        return View::fetch("/post-list");
    }

    public function category()
    {

        $list = Db::name("category")->select();

        View::assign("list",$list);
        return View::fetch("/cate");
    }

    public function delPost()
    {
        $id = input("post_id");
        $del = P::destroy($id);
        if ($del){
            return json(["code" => 1,"msg" =>"删除成功"]);
        }
        return json(["code" => 0,"msg" =>"删除失败"]);
    }

    public function postAdd()
    {
        $id = input("id");
        if (request()->isAjax()){
            $data = input("post.");
            $str="Line1\nLine2\rLine3\r\nLine4\n";
            $order=array("\r\n","\n","\r");
            $replace='<br/>';
            $data["content"]=str_replace($order,$replace,$data["content"]);

            try {
                validate(postValidate::class)->check($data);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return json(["code" => 0,"msg" =>$e->getError()]);
            }

            if (empty($id)){
                preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i',input("content","","trim"), $ereg);//正则表达式把图片的SRC获取出来了
                if(empty($ereg)) {
                    $cover_image = "";
                }else{
                    $cover_image = $ereg[1];//图片SRC
                }
                $data["cover_image"] = $cover_image;
                $data["uid"] = session("AdminId");
                $create = P::create($data);
                if ($create){
                    return json(["code" => 1,"msg" =>"发布成功"]);
                }
                return json(["code" => 0,"msg" =>"发布失败"]);
            }else if ($id > 0){
                $update = P::update($data);
                if ($update){

                    if ($data["status"] == 0){
                        $wxSend = new Wechat();

                        $cataName = Db::name("category")->where("cate_id",$data["cate_id"])->value("cate_name");
                        $openid = Db::name("user")->where("uid",session("UID"))->value("openid");
                        $templateId = "U6ptqmnXmBRZzOc1J2UuBMV49mG5ZQqOxv-MVqYiMUU";
                        $first =  ["value" =>"您的文章《" .$data["title"] ."》已通过管理员审核，可登陆:".Request::domain()."查看","color" => "#173177" ];
                        $keyword1 =  ["value" =>$cataName,"color" => "#173177" ];
                        $keyword2 =  ["value" =>date("Y-m-d"),"color" => "#173177" ];
                        $keyword3 =  ["value" =>"4","color" => "#173177" ];
                        $remark =  ["value" =>"如有疑问请公众号内留言","color" => "#173177" ];

                        $wxSend->wxSendMember($openid,$templateId, $first,$keyword1,$keyword2, $keyword3,$remark);
                    }

                    return json(["code" => 1,"msg" =>"修改成功"]);
                }
                return json(["code" => 0,"msg" =>"修改失败"]);
            }
        }else{
            $postDetail = P::find($id);
            $category = Db::name("category")->select();
            View::assign("category",$category);
            View::assign("postDetail",$postDetail);
            return View::fetch("/post-add");
        }
    }

    public function cateAdd()
    {
        $cateName = input("cateName");
        if (empty($cateName)){
            return json(["code" => 0,"msg" =>"分类名不能为空"]);
        }
        $rel = Category::create(["cate_name" => $cateName]);
        if ($rel){
            return json(["code" => 1,"msg" =>"添加成功"]);
        }
        return json(["code" => 0,"msg" =>"添加失败"]);
    }

    public function delCate()
    {
        $id = input("cate_id");
        $del = Category::destroy($id);
        if ($del){
            return json(["code" => 1,"msg" =>"删除成功"]);
        }
        return json(["code" => 0,"msg" =>"删除失败"]);
    }

    public function updateCate()
    {
        $cateId = input("cate_id");
        if (request()->isAjax()){
            $cateName = input("cate_name");
            $rel = Category::update(["cate_name" => $cateName],["cate_id" =>$cateId]);
            if ($rel){
                return json(["code" => 1,"msg" =>"修改成功"]);
            }
            return json(["code" => 0,"msg" =>"修改失败"]);
        }else{
            $cateDetal = Db::name("category")->where("cate_id",$cateId)->find();
            View::assign("cateDetail",$cateDetal);
            return View::fetch("/update-cate");
        }
    }
}