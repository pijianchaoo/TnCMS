<?php

namespace app\common\model;


use think\Model;

class User extends Model
{
    protected $pk = 'uid';
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;
    public function group()
    {
        return $this->hasOne("Group","group_id","group_id");
    }
}