/*
 Navicat Premium Data Transfer

 Source Server         : v6_tn721_cn
 Source Server Type    : MySQL
 Source Server Version : 50637
 Source Host           : 123.207.55.247:3306
 Source Schema         : v6_tn721_cn

 Target Server Type    : MySQL
 Target Server Version : 50637
 File Encoding         : 65001

 Date: 24/10/2019 19:24:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tn_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_category`;
CREATE TABLE `tn_category`  (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cate_cover_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_category
-- ----------------------------
INSERT INTO `tn_category` VALUES (1, 'php开发', NULL);
INSERT INTO `tn_category` VALUES (2, '网络营销', NULL);
INSERT INTO `tn_category` VALUES (3, '自媒体', NULL);
INSERT INTO `tn_category` VALUES (4, '微信营销', NULL);

-- ----------------------------
-- Table structure for tn_group
-- ----------------------------
DROP TABLE IF EXISTS `tn_group`;
CREATE TABLE `tn_group`  (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_group
-- ----------------------------
INSERT INTO `tn_group` VALUES (1, '超级管理员', 1587854384);
INSERT INTO `tn_group` VALUES (2, '默认用户组', 1568803859);

-- ----------------------------
-- Table structure for tn_group_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_group_rule`;
CREATE TABLE `tn_group_rule`  (
  `group_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_group_rule
-- ----------------------------
INSERT INTO `tn_group_rule` VALUES (1, 28);
INSERT INTO `tn_group_rule` VALUES (1, 15);
INSERT INTO `tn_group_rule` VALUES (1, 38);
INSERT INTO `tn_group_rule` VALUES (1, 4);
INSERT INTO `tn_group_rule` VALUES (1, 1);
INSERT INTO `tn_group_rule` VALUES (1, 31);
INSERT INTO `tn_group_rule` VALUES (1, 11);
INSERT INTO `tn_group_rule` VALUES (1, 16);
INSERT INTO `tn_group_rule` VALUES (1, 9);
INSERT INTO `tn_group_rule` VALUES (1, 23);
INSERT INTO `tn_group_rule` VALUES (1, 7);
INSERT INTO `tn_group_rule` VALUES (1, 12);
INSERT INTO `tn_group_rule` VALUES (1, 13);
INSERT INTO `tn_group_rule` VALUES (1, 14);
INSERT INTO `tn_group_rule` VALUES (1, 2);
INSERT INTO `tn_group_rule` VALUES (1, 8);
INSERT INTO `tn_group_rule` VALUES (1, 17);
INSERT INTO `tn_group_rule` VALUES (1, 18);
INSERT INTO `tn_group_rule` VALUES (1, 19);
INSERT INTO `tn_group_rule` VALUES (1, 20);
INSERT INTO `tn_group_rule` VALUES (1, 21);
INSERT INTO `tn_group_rule` VALUES (1, 22);
INSERT INTO `tn_group_rule` VALUES (1, 10);
INSERT INTO `tn_group_rule` VALUES (1, 24);
INSERT INTO `tn_group_rule` VALUES (1, 25);
INSERT INTO `tn_group_rule` VALUES (1, 26);
INSERT INTO `tn_group_rule` VALUES (1, 29);
INSERT INTO `tn_group_rule` VALUES (1, 5);
INSERT INTO `tn_group_rule` VALUES (1, 27);
INSERT INTO `tn_group_rule` VALUES (1, 30);
INSERT INTO `tn_group_rule` VALUES (1, 6);
INSERT INTO `tn_group_rule` VALUES (1, 32);
INSERT INTO `tn_group_rule` VALUES (1, 33);
INSERT INTO `tn_group_rule` VALUES (1, 34);
INSERT INTO `tn_group_rule` VALUES (1, 35);
INSERT INTO `tn_group_rule` VALUES (1, 36);
INSERT INTO `tn_group_rule` VALUES (1, 37);
INSERT INTO `tn_group_rule` VALUES (1, 3);
INSERT INTO `tn_group_rule` VALUES (1, 39);
INSERT INTO `tn_group_rule` VALUES (1, 40);
INSERT INTO `tn_group_rule` VALUES (1, 41);
INSERT INTO `tn_group_rule` VALUES (1, 42);
INSERT INTO `tn_group_rule` VALUES (1, 43);
INSERT INTO `tn_group_rule` VALUES (2, 7);
INSERT INTO `tn_group_rule` VALUES (2, 33);
INSERT INTO `tn_group_rule` VALUES (2, 34);
INSERT INTO `tn_group_rule` VALUES (2, 35);
INSERT INTO `tn_group_rule` VALUES (2, 36);
INSERT INTO `tn_group_rule` VALUES (2, 37);
INSERT INTO `tn_group_rule` VALUES (2, 38);
INSERT INTO `tn_group_rule` VALUES (2, 39);
INSERT INTO `tn_group_rule` VALUES (1, 44);

-- ----------------------------
-- Table structure for tn_link
-- ----------------------------
DROP TABLE IF EXISTS `tn_link`;
CREATE TABLE `tn_link`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `link_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(10) NULL DEFAULT 0,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`link_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_link
-- ----------------------------
INSERT INTO `tn_link` VALUES (1, 'TnCMS', 'https://www.tn721.cn', 0, 1597845682);
INSERT INTO `tn_link` VALUES (2, 'ThinkPHP', 'http://www.thinkphp.cn', 0, 1569659999);
INSERT INTO `tn_link` VALUES (3, 'TnCMS', 'https://www.tn721.cn', 1, 1571828997);
INSERT INTO `tn_link` VALUES (4, '企业官网', 'https://www.tn721.cn', 2, 1571829059);
INSERT INTO `tn_link` VALUES (5, '官网发布', 'https://www.tn721.cn', 3, 1571829086);
INSERT INTO `tn_link` VALUES (6, '关于我们', 'https://www.tn721.cn', 4, 1571829188);
INSERT INTO `tn_link` VALUES (7, '联系我们', 'https://www.tn721.cn', 4, 1571829195);

-- ----------------------------
-- Table structure for tn_nav
-- ----------------------------
DROP TABLE IF EXISTS `tn_nav`;
CREATE TABLE `tn_nav`  (
  `nav_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nav_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `order` int(255) NOT NULL DEFAULT 100,
  PRIMARY KEY (`nav_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_nav
-- ----------------------------
INSERT INTO `tn_nav` VALUES (1, 0, '网站建设', '/', 100);
INSERT INTO `tn_nav` VALUES (2, 0, '网络营销', '/', 100);
INSERT INTO `tn_nav` VALUES (3, 2, '微信营销', '/', 100);
INSERT INTO `tn_nav` VALUES (4, 2, '自媒体', '/', 100);

-- ----------------------------
-- Table structure for tn_post
-- ----------------------------
DROP TABLE IF EXISTS `tn_post`;
CREATE TABLE `tn_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status` int(2) NULL DEFAULT 0,
  `cate_id` int(11) NOT NULL,
  `cover_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `uid` int(11) NULL DEFAULT NULL,
  `read_count` int(255) NULL DEFAULT 0,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_post
-- ----------------------------
INSERT INTO `tn_post` VALUES (14, '自媒体月入过万，对不起，别再被收智商税了', '<p><br/></p><p>做公众号半年多了，这是我坚持得最久的一件事。程序员出身，做过网站，不，是做过好多个网站，但都是三分钟热度，不到两周内，全部关停。也做过小程序，也就是公众号里面的【写意读书】【精致好物】等这几个小程序。当初信了雷军的鬼话：“站在风口上，猪都能飞起来”，于是去做了小程序这个风口，但现在明白了，风口也是有方向的，自己翅膀不硬，在风口上飞得越高，摔得越惨。</p><figure><img src=\"/uploads/postImages/20191022/b828c237bb709aa972c3e9e54e7bfc0d.jpg\" style=\"max-width:100%;\"></figure><p>公众号做到现在，有来自豆瓣，简书的用户，而且大部分都是喜欢写写文章，也有一些是做公众号的朋友。但我相信不管是做公众号还是写文章的朋友，都是想通过做这些事情，得到一些收益或者得到别人的认可。</p><p>为什么我会开一个公众号呢？我也说不清楚，大概是当时逛简书的时候，看见好多人都在开公众号，于是也很好奇的开了一个。后面慢慢了解到别人的公众号都接了广告赚钱，了解到一个90后的小姐姐因为开了自己的公众号，开启了自己的自由职业之路。有了心目中的偶像，那当然是干劲十足了，于是我后面又开了两个公众号，但那两个公众号到现在更文都不超过两篇，已经属于放养状态了，唯一坚持下来的只有这个公众号。</p><p>2018年可谓是自媒体元年，各大自媒体平台不断出现，百家争鸣，于是便有很多人开始自己的自媒体生涯之路，我也是其中之一，注册了百家号，头条号等等各种自媒体平台，平台注册下来了，到底该写什么呢？怎么去吸引用户呢？于是做了没多久，便都停更了。</p><p>在网络上经常看到一些做自媒体月入上万等等的鸡汤文章，也会偶尔点开去看，别人说的也不是没有道理，但做事情还是需要打铁自身硬的。我不相信那些写月入上万的人能真正的月入上万，那些只是割韭菜的人，来收智商税的人。我相信能月入上万的人是不会出来跟你说怎么做才能月入上万的，因为他们很忙，世界上没有那么多雷锋。当然做自媒体能月入上万的人也是有的，任何事情肯定会遵循“二八定律”，但其中“二”的那一部分，肯定是坚持下来，并且有自己干货的人。</p><p>说了那么多，我想表达的主题就是，不管你是写手，自媒体从业者，还是做其他事情也好，做事情千万不能浮躁，特别是做自媒体，做公众号的朋友，你要努力，但不要着急，粉丝经济是慢慢积累的过程，坚持下去，努力做好内容才是王道。</p><p>我觉得写内容是最需要灵感的一件事，如果你满脑子里都想着该写什么东西呢，写什么内容用户更加喜欢看呢，这样你是写不出一篇好文章的。好的内容一定是来源于生活，也许你今天去做公交车，发生了一件很有趣的事情，晚上和朋友去看电影，从电影里感悟到了什么，或者你吃着午餐的时候脑子突然灵光，产生了一些奇思妙想等等。</p><p>总之，不要浮躁，该吃吃，该喝喝，该玩玩，该努力努力，好的东西需要慢慢沉淀的。</p><p>之前看见有一个人在网上问：“现在自媒体发展是不是已经成熟了？新人还有机会吗？”，个人认为目前的自媒体还只是爆发阶段，有各式各样的人或者团队进入自媒体，还处于百家争鸣的阶段，尚未形成格局，所以新人还是有机会的，只要有内容，坚持做下去，还是有机会的。只要你有才华，随时都是你成长的乐园。</p><br/><br/>', 0, 2, '/uploads/postImages/20191022/b828c237bb709aa972c3e9e54e7bfc0d.jpg', 1, 18, 1571732936);
INSERT INTO `tn_post` VALUES (15, '亲身经历讲述2019年公众号还能挣到钱吗？', '<p><br/></p><p>现在开始做公众号到底晚了没有呢？</p><p>确实是有点晚了，但并不知道代表不可以做了，他只是过了这个风口。</p><p>在当下微信占领了几乎一半的移动流量入口，而公众号是微信主打产品之一，因此在很长一段时间里，公众号都处于红利期。有人可能会说，现在是短视频时代，谁还来看图文的东西，的确短视频占据了很大一部分用户的时间。但短时候是娱乐性的产物，它并不能代替图文。就像电纸书代替不了纸质书籍一样。2018年开始，很多人都在唱衰公众号，说公众号已经不行了，红利期已经过了。但是，<br/> 2018年开始做公众号的有些人，还是赚到了钱。</p><p>我也是2018年8月份才开始做的公众号，当时我也很迷茫，到底现在做公众号是不是晚了。我也很好奇的去百度搜了下，发现有个2016年的回答，它说自媒体的红利期已经过了，已经饱和了，也有2018年回答，也是说公众号饱和了，不适合做。但是我也在百度上看到17年和18年做起来的公众号，有成为大号的。</p><p>自媒体的关键点是内容，内容为王，只要你内容质量上去，什么时候做都不晚。</p><p>说一下我做公众号的个人经历吧</p><p>我公众号是2018年8月份开始做的，从零开始，没有任何一个粉丝。我写了第一篇文章，阅读量只有我自己，后来去做了一些推广，去其他平台发文章，慢慢的有了几个粉丝。</p><p>就这样我坚持做了下去，到了2018年10月份的时候，粉丝大概有了1000人左右。但是粉丝活跃度有些低，文章的阅读量总是上不去，大概只有70左右。</p><p>后面请教了一些前辈，慢慢的了解该如何去推广，吸粉，提高粉丝的活跃度。</p><p>曾经因为推广没有效果想过放弃，但是放弃又觉得心疼，毕竟运营了那么几个月。</p><p>后面还是坚持了下来，在这里告诫做公众号的朋友们，做公众号是慢慢积累的过程，需要沉淀，才会有收获。</p><p>“人但有追求，世界亦会让路”，努力并坚持做下去，才会有收获，这也是我公众号的简介。</p><figure><br></figure><p>虽然目前粉丝也不多，但如果你也是做公众号的，欢迎你来和我交流，一起见证双方的成长。</p><p>公众号内整理有一些详细引流的方法，关注后免费送你哦，不是百度那种千篇一律的教程，是自己根据经历所写。</p><br/><br/>', 0, 4, '', 1, 0, 1571733003);
INSERT INTO `tn_post` VALUES (16, '新手0到1000粉丝运营涨粉思路 ', '<p><br/></p><p>有粉丝，就能接广告，就能养活自己。</p><p>那如何引流呢？</p><p>对于公众号引流涨粉这件事，很多同学嘴上说着佛系心里却很诚实，想要涨粉但又不知道该怎么办，这样的同学你可以往下阅读，纯干货，不废话，良心分享。</p><p>（以下为自己总结的公众号推广思路，只针对小白，大佬请出门左拐）</p><p><strong>一、知乎引流</strong></p><p>知乎想必大家都是了解的，这里聚集着可能是目前为止网络上质量最高的一批粉丝群体。 截至目前注册用户已经达到1.6亿。知乎里聚集着各个行业的精英人物，不管是什么行业的人群，都能从这个平台中引流。</p><p>小白应该如何玩转知乎呢？</p><p>注册好知乎账号后，首先要做的就是修改自己的帐号资料，在上面增加自己的公众号资料，达到让别人关注你公众号的目的。</p><p>我们不是拥有上万粉丝的大佬，所以一定要在答题的选择上下功夫，否则没人点赞没人看得到，等于白写。</p><p>知乎的热榜都知道吧，在热榜的问题的确有很多人在回答，也有很多人在看，可是答题之后呢，有很大可能你的回答就被淹没在铺天盖地接踵而至的答案里了。</p><p>所以，纯纯的小透明，不建议去回答热榜，我的建议是：回答那些频繁出现在首页的问题，而且要观察，有没有人在认真看。</p><p>点进一个问题，发现高赞答案里有这几天的新答案；按时间排序查看，如果最新的几条回答都有零星点赞和评论的话，那么这道题就有回答的价值。</p><p>一个高赞的答案回答字数控制在800-1500字左右，内容尽量合理并且有自己的观点，能帮助到提问者，回答的问题一定要原创，不要去网上复制粘贴过来。</p><p>在知乎发的作品在百度有很高的权重，百度搜索的时候你的文章会显示的比较靠前，利于引流百度搜索的用户。</p><p>二、豆瓣引流</p><p>说起豆瓣，大家应该都不陌生，它是一个以兴趣分类的用户聚合平台，读书，电影，音乐，工作，兼职，写作等等，只有你想不到的兴趣群。这么大的一个用户鱼塘，能不能钓到鱼，那就看你自己的本事了。</p><p>想去豆瓣大池塘里钓鱼，你一定要清楚自己要钓什么样的鱼，你要钓的鱼喜欢吃什么鱼饵。比如你是做资源分享类型的公众号，你就可以去搜索资源关键字。</p><figure><br></figure><p>我搜索了“电影资源”，“资源”这两个关键字，可以出来很多相关的小组，这时候你要做的就是加入这些小组，去分享你的资源并且将这些需要资源的用户导入到你的公众号中，当然不能有过分的广告嫌疑，否则有被小组封禁的可能，要准守小组的规则才能长期“打广告”。</p><p>这只是举个例子，如果你是做其他方面的内容，你也可以搜索相关的小组，然后加入进去，发一些帖子，让别人关注你，达到引流的目的。</p><p>在豆瓣可以发布日记，你可以发布日记，广播到各个相应的兴趣话题里面，如果你的文章能够打动人，有干货，那么你公众号涨个1000粉只是分分钟的事情。</p><p>三、简书引流</p><p>简书可能名气就没有那么大了，部分同学会没有听说过。在我心目中，简书就是文艺青年的集聚地，有很多的大学生和刚出来社会不久的青年在上面发表文章。因为发布文章的门槛低，所以文章的质量也是参差不齐。我第一个引流的平台就是简书，第一个1000粉丝就是从简书而来的。简书的引流是没有任何套路可言的，靠的全是对文字满腔的热爱，通过发布一篇又一篇的文章来引起读者的关注。</p><p>我原创写的文章也不多，阅读量高的就两篇，都是1000阅读量左右。</p><p>我在文章的末尾和赞赏的设置写上了关注公众号的标语，达到引流的作用，如果你的文章内容质量高，蹭热度，有干货，阅读量高的话，那公众号涨粉是很快的，并且简书引流到公众号的用户黏性都比较高，比较活跃。</p><figure><br></figure><p>四、公众号互推</p><p>互推对于做公众号的人应该是很熟悉的词。言简意赅 ，就是去找和自己类型相同的公众号做推广，互相开白名单，互发对方的文章。</p>', 0, 4, '', 1, 4, 1571733076);
INSERT INTO `tn_post` VALUES (17, '知乎涨粉、实战引流指南 ', '<p><br/></p><p>说起知乎，我得感谢一位朋友带我玩的，我看着他知乎从一千二的粉，做到现在九千多粉，离破万也不远了。</p><p>进入一个陌生的行业和领域，如果有个做的好的人带路，绝对能最快时间在这个行业扎根!</p><p>每个平台都是不一样的，知乎一万粉的含金量已经不小了。</p><p>纵然大家对知乎的印象很多都是：逼乎，分享你刚编的故事。但迄今为止，知乎仍然是对内容<a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/tags/chuangye.html\" target=\"_blank\" rel=\"noreferrer noopener\">创业</a>者包容度较高的一个平台。</p><p>由于其用户的高净值，高学历的特点，只要你足够专业，流量就会非常精准。</p><p>知乎粉丝最高的仅仅两百多万粉，微博粉丝最高的一个亿，抖音粉丝最高的五六千万吧。</p><p>我这朋友靠他这几千粉，每个月能赚几千块。点赞，接广告，写软文营销等。</p><p>首先谈一个权重问题，每个平台都有账号权重，千粉，万粉都是高权重账户，知乎的圈子里，很多人花钱买千粉给他点赞，点赞越多，他这个回答就会越火，获得更多的推荐和曝光，所以点赞值钱!</p><p><a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/reed/12064.html\" target=\"_blank\" rel=\"noreferrer noopener\">养号</a>：很多平台都需要养号，所谓养号其实就是让自己像是一个活人，正常用户。完善好账户信息，该设置的设置好，千粉之前不要做任何引流，不要留任何联系方式等!</p><figure><br></figure><p>在我脑子里，把它分为两大部分：</p><p>①、一千粉之前</p><p>②、一千粉之后</p><h3>①一千粉之前</h3><p>千粉之前不要做引流，不要做任何违规的事情，保持日更，每天最少回答一个问题，回答的内容最好一百字以上!</p><p>回答这块，只回答热榜的问题，热榜里面有排名。排名里1到8的不要回答，只回答8到35之间的问题!</p><p>首先回答热榜的原因，是因为可以蹭热度，给你的回答带来流量，然而普通邀请你回答的问题，多数是石沉大海，写了也没人看得到!</p><p>排名1到8的问题，热度比较大，回答的人也比较多，热度上升有限了，不建议回答!</p><p>排名8到35的呢，回答的人少一些，热度却一直在逐渐上升，流量比较大，更容易得到曝光!</p><p>千粉以前，精髓是日更，只写热榜!</p><h3>②一千粉之后</h3><p>接下来是重点，敲黑板，做好笔记!</p><p>千粉之后，玩报团取暖，开始混知乎的圈子，进这方面的QQ群，或者微信群。</p><p>这类群最低门槛就是千粉，也就是自己有了一定的实力，就可以和他们报团取暖!</p><p>前面说了，千粉，万粉都是有权重的号，给你点个赞都可能让你获得流量，珍贵的很!当你也有千粉的时候，便能和千粉合作，相互点赞，相铺相成!</p><figure><br></figure><p>获得点赞有两种：<strong>一个是花钱买，一个是相互点赞!</strong></p><p>加和你粉丝差不多的十个人，谈好合作，长期合作啥的，吃透高权重点赞这一点即可!</p><p>千粉的时候就混千粉的圈子，万粉的时候就混万粉的圈子，万粉比千粉权重更高，一步一步做大做强!</p><p>问：我是小白，我没有这方面的资源，如何进入这方面的圈子，微信群?</p><p>答：千粉以后，你开始在知乎加千粉的同行，和他们交朋友，套近乎，问他们有没有这方面的群，有的话拉一下你，红包答谢!</p><p>加几十个同行，问群，如果运气太差，都没有问到。当你有了几十个同行，自己开个群就好了，基础人数就有了!</p><p><strong>联合一切能联合的人，整合一切能整合的资源!</strong></p><p>以上是我做知乎摸索出来的经验和技巧，我所有文章都是经过实战，经得住考验的方法，没有做过的东西，我绝不会吹牛逼写出来，从来不玩虚的，实战为王!</p><p>最后再总结下，想要做好个人品牌，始终要遵从四个字：内容为王。</p><p>如果你要引流，必须不断输出自己在该领域内的价值，持续进步和精进。定位+垂直+坚持，这个原则适用于所有平台。</p><p>作者/公众号：纵横领域</p><br/><br/>', 0, 3, '', 1, 11, 1571733111);
INSERT INTO `tn_post` VALUES (18, '关于私域流量引流渠道与软文选题的一些思路 ', '<p><br/></p><p>想要精准引流，第一个要做的是了解粉丝的想法。</p><p>所以首先要通过<a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/tags/baidu.html\" target=\"_blank\" rel=\"noreferrer noopener\">百度</a>指数、站长之家、360关键词查询、新榜等，去查询这阶段用户在关注什么、同行在推什么、哪些文章成为了爆款，同时把每次查询总结出来的结果记录下来，以月为单位进行汇总，这样一个项目整个生命周期的所有关键点、如何选题就都掌握下来了。</p><p>选题确定好之后，就要考虑哪个渠道应该推什么样的内容。</p><p>这里就需要先把自己想想成用户，然后从用户的角度去思考当他遇到问题时、想要考证时会先怎么做、再怎么做、还会怎么做。</p><p>比如说，今天你想考英语四级，你会怎么做?无非就是两种，一是报班、二是自学。如果你想报班，那又会延展出两个方向，一是我的朋友中有没有报过的;二是有没有哪些比较有名的培训机构。</p><p>从这个角度出发就延展出<strong>两条引流思路：</strong></p><p><strong>第一条做好我现有粉丝群体的关系打造，</strong>比如加到我微信号上的用户，我要经常关怀一下、他更新了朋友圈我要及时给他点赞一下、把他带我我的群里，定期给他提供一些免费的资料等等。</p><p><strong>第二条就是做好品牌的宣传，广告的铺垫占领用户的心智，</strong>让他有需求时第一个想到的就是你。我之前说过，广告就是“广而告之”即使再优秀的广告也只有三分之一是有作用的，但是我们却不能知道是哪三分之一会产生作用，所以广告拼的就是覆盖量、包装效果。</p><figure><img src=\"https://images.lusongsong.com/zb_users/upload/2019/09/201909063499_677.jpg\" alt=\"私域流量推广指南 互联网 流量 自媒体 经验心得 第1张\"></figure><p>我们再回到开头的问题上，你除了报班还有可能自学是不是?如果要自学的话又分为两个渠道，第一个你总要买些教材什么的吧?那就延伸出了淘宝、京东、闲鱼等等，这些电商平台汇集了我们的用户群，那么我们是不是就可以第一扩展网店;第二对一些电商平台的流量论坛铺设推广信息?</p><p>除了买教材外你是不是也会百度搜一下是不是有免费的学习资源?所以我们还应该关注，哪些平台的文章更容易被收录、哪些平台用户信任度比较高、哪些平台可以展现联系方式。我之前让大家做天涯、做博客、做搜狐，这些平台的流量都不高。但是这些平台的权重却很高，所以我们依然要重视。其次就是当你刷抖音、刷头条、刷新闻的时候，如果看到和四级相关的内容，你是不是也会点开看一下?所以类似这些流量比较大的自媒体平台我们也需要做一下。</p><p>当我们决定通过内容产出来进行引流时，最需要解决的是如何撰写引流话术，让用户能够去添加你的微信。这些是我们要做的重点，至于说我的内容要如何优化，我遣词造句要如何，虽然也很重要，但我认为不是我们的重点。而且我们也不要去祈求如何去培养这些平台上的粉丝，因为你很难让他长时间的去关注你。所以唯一要做的就是如何尽快把他加到你的微信上去。</p><p>腾讯是天然的大流量池，想一下，如果你要自学、你会不会加一些学习群?一定会的。所以，第一个就是我们的群要做好<a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/reed/11972.html\" target=\"_blank\" rel=\"noreferrer noopener\">群排名优化</a>、要做好群流程、做好对接话术;第二个同行的群要多加，每天都要加两个。第三个同行的公众号、活动要多关注，要尝试打入同行内部去。</p><p>当你从粉丝的角度把这一块给梳理清楚后，你可以确定哪些东西呢?第一个我要做哪些渠道、第二个我要推哪些内容、第三个我要把用户引流过来。这里提一下，引流效果的排序依次是：视频、文案、音频。所以<a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/reed/10060.html\" target=\"_blank\" rel=\"noreferrer noopener\">如何打造你的视频内容</a>、如何打造你的文案内容，将是决定引流成功与否的重要因素。</p><h3>软文撰写内容的第一步，<a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/reed/12168.html\" target=\"_blank\" rel=\"noreferrer noopener\">标题</a></h3><p>内容这一块我主要讲两点，第一个是标题、第二个是引流话术。因为我们不是专门去做编辑或者专门写作，所以文章是不是原创，是不是通顺等等是不太重要的。</p><p><strong>关于标题第一个要说的是紧扣关键词，</strong>人永远只对他关心的事感兴趣，所以我们前面通过查词工具选出来的关键词，在这个环节就要用上了，而且在前15个词上要尽量多的堆积上关键词。</p><p>比如说一个要考英语四级的他们现在比较关注什么，那你标题上就要体现什么。</p><p><strong>第二个你的标题要能引起读者情绪的波动，</strong>那么怎样才能引起读者情绪波动呢?比如：改陈述句为疑问句;善用标点符号等等。</p><p><strong>第三个越精准越能增加可信度，</strong>所有人都希望看到的内容是有价值的干货，那么如何凸显出我们的内容是有价值的呢?这就需要在标题中提现出内容的逻辑性、高效性。</p><p><strong>第四点要学会多种语式套用，</strong>比如，正反对比、自问+自答+强调、疑问+紧迫感等等。</p><p>然后还有一些标题的类型，比如独特性的、盘点性的、借助名人或者热点的，大家也都可以尝试着写一写，总之，<strong>你的标题一定要反复优化、让每一个字都有用、每一个字都是为了引导用户点开你的内容，</strong>那这个标题就可以了。</p><h3>如何撰写内容</h3><p>常见的软文形式包括：悬念式、故事式、情感式、恐吓式、促销式、新闻式等等。这些软文又该怎么写呢?</p><p><strong>第一点就是多看，</strong><a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/tags/hulianwang.html\" target=\"_blank\" rel=\"noreferrer noopener\">互联网</a>发展到今天，各种各样的软文形式都有优秀的案例，比如说大家熟知的：蓝翔、新东方、脑白金等等。现在流传的很多段子都是他们创造的引流软文，而且这些软文所产生的影响已经不仅仅是引流这么简单了，它是以极低的成本为企业打造的一场成功的品牌宣传、而且很少有广告能像它们这样影响这么多年，除了“钻石恒久远，一颗永流传”</p><p><strong>第二点要尽量口语化、</strong>一步一步引导用户进入到你的文章中去，而不是生硬地打广告。以悬念式软文为例，开头往往会抛出1/2/3/4个疑问，然后让你带着这些问题往下读，然后抛出若干个论据来诠释观点，每一个观点都尽量控制文章字符，让读者不太累，愿意一点点看下去。最后通过你的论点、论据等论证出要考消防工程师，需要用这套资料，而且这套资料非常贵，但是恰巧你有，而且因为某件原因要免费送，那用户就水到渠成地加到你微信上去了。</p><p><strong>最后就是善于去归纳、发现，</strong>比如今天范冰冰结婚了，你想以此热点去写一篇软文，你应该先干什么呢?应该先去挖掘这个这个热点背后产生了哪些关键词，然后把这些关键词归纳总结出来，在去看这个时间段，用户关注的点是什么?最后就可以确定我要用那一种表达形式来撰写了，比如说以这件事展开的新闻式、以范冰冰的故事展开的故事式、以范冰冰为跳板去阐述一个女人，正面的、反面的等等各种方面的情感式都可以创作出一篇优秀的软文。</p><p>所以说我们的文章类型要敢于突破，不能只是集中在一两个点上去撰写，而要天马星空、不断尝试。</p><p><strong>总之，软文这一块总结为一下几句话：</strong></p><p>1 、 逻辑要清晰、内容要精练</p><p>2 、尽量口语化、要足够的软</p><p>3 、要从用户角度出发，避免陷入盲目自嗨。</p><h3>我们优化标题，我们撰写软文目的是什么呢——引流话术</h3><p>为了让用户联系我们，所以引流话术就显得特别重要。</p><p><strong>关于话术首先要讲的就是要和我们的软文进行融合。</strong>不是强行植入，同时要统筹布局，在文章的合理位置插入关键词，进行串联。</p><p><strong>然后就是口语化用词。</strong>举个例子，牌子和品牌两个词用作引流时你会用哪个?正常情况下你用牌子效果会更好，因为牌子更加口语化、更加符合大部分人的生活，所以要善于利用搜索下拉框、相关搜索、相关推荐等等，多看同行机构的信息流广告，然后组合自己的引流话术。</p><p><strong>其次就是尽可能多的展示你的引流话术。</strong>比如说多去评论别人的内容、抢一楼，然后字数尽可能多一些、有针对性些、真诚一些。让用户相信你，或者对你的内容感兴趣，才能把用户吸引过来。比如说“想要考下英语四级我认为1、2、3、【阐述一下】我是xxx，一个xxx，如果你想xx可以xxx”比如你做了一个引流文章被收录了，那你就可以用关键词的方式去引导，举例，经常会看到的千万不要用百度搜索“xxx”.....就是利用这样的技巧。</p><p>最后总结一下引流的本质就是：<strong>去人多的地方、提供有价值的内容、引导进入自己的池子。</strong>所以当你去布局一篇引流文章的时候，你也要思考一下，我的用户是谁，他对什么东西感兴趣，他会在哪里看到这个东西，我应该如何去引导他。当你把人性的七宗罪都运用上的时候，流量自然就来了。</p><h3>最后我们再讲一下具体到各平台又该如何去引流</h3><p>其实前面讲到的这些在各平台都是通用的，但是又都有不同的地方，所以说我们做引流的时候。针对每一个平台都要仔细研究平台规则、实验各种的推广方法。然后针对具体平台的引流，我主要针对大家问到的渠道，写了三个板块，第一个是知乎。第二个是头条，第三个是百度系列。</p><figure><img src=\"https://images.lusongsong.com/zb_users/upload/2019/09/201909063673_906.jpg\" alt=\"私域流量推广指南 互联网 流量 自媒体 经验心得 第2张\"></figure><p><strong>先说知乎：</strong></p><p>关于知乎首先要明确的一点是我们做知乎的引流一定不要去追求爆文，因为爆文是不确定性的，知乎的引流更追求稳定的产出，是执行力。</p><p>其次知乎的权重比较高、粉丝比较精准，所以这个渠道如果做好的话，会让我们省很大的力气。知乎必须要利用好个人介绍“头像、昵称、描述、个人介绍、行业、居住地”等等必须要完善，而且要凸显出自己的不同。</p><p>关于知乎能够引流的地方，我大致测试了一下包括：主页、回答开头、回答的过程中、回答的底部、回答的评论中，这些地方都可以设置引流内容。其中主页可以有36个字，但是回答别人时，别人只能看到14个字，所以前14个字必须要凸显出引流内容。</p><p>然后就是职业经历、教育经历这个版块可以放微信号，所以知乎的这些基本信息上必须要完善到位。其次，知乎和百度知道、悟空问答类似，所以把这个搞懂了，类似的渠道也可以套用上去。</p><p>关于知乎引流主要分为写文章提问以及问答两种。回答的话又需要注意，选择合适的问题进行回答。</p><p>首先，就是关键词的精准度，我们判断一个问题值不值得回答时，可以先看一下这个问题选择的关键词包括哪些。</p><p>其次还可以参考评论，看是不是高评论的，是不是最近一个月内的，然后通过话题去筛选问题。</p><p>最后知乎的高赞文章是有一定套路的。当你用上这种套路的时候，你的引流效果一定是比普通的好的。</p><p>具体的套路类似这样：</p><p>泻药，我感觉楼上所有人回答的都烂透了。</p><p>人在广州，刚给考生上完课，简单说下我对如何备考的看法</p><p>【凸显身份、逼格】</p><p>然后分割线;</p><p>多图预警、</p><p>多图预警、</p><p>多图预警，</p><p>重要的事情说三遍;</p><p>【阐述观点、配图佐证】</p><p>通过我这套方法已经拿证的现在至少也有xxx，</p><p>月收入至少在xxx左右;圈内人太多，匿了。</p><p>【分割线】</p><p>没想到我这篇文章反响这么大，好多人在酸，反正信不信由你，</p><p>不服来辩。我只能说xxxx</p><p>【分割线】我微信号是：xxxx</p><p>想了解或者准备考的人，可以联系我。</p><p>我在详细给你说下，在这里说又有很多人开始酸了。</p><p>大概就是这样一个格式。</p><p>然后在撰写文章的时候，还可以在开头吸引用户，比如资料包【文字+图片凸显】比如写在前面，文末有小惊喜等等。</p><p>总之，知乎的引流一定要系统化、一定要体系化。最好针对每个问题准备一个表格，然后把这个问题方方面面可能的回答都列出来，最好选择一个去主攻。知乎的话对故事性的文章欢迎度比较高，所以打造一批有故事、而且是高真实度、配图佐证的文章效果会比较好。比如从自己是如何考下消防工程师的，然后一步步具体写怎么做，凸显专业，用故事证明自己的观点，最后如果想考的可以加我，或者关注什么，用来引流。然后知乎的引流话术，要相对简洁一点。具体可以为：在哪里+提供什么+有什么效果;</p><p><strong>头条：</strong></p><p>关于头条的话就相对比较简单了，上面讲到的所有方法都可以套用进来。但是要注意两点;第一个文章中，包括文章的配图中都不要出现微信、QQ等联系方式，必须要引导用户私信你，并且必须要用户先私信你，然后再私信引导添加你微信，或者在评论区留下联系方式。还有就是要多关注别人，并且在别人文章下做评论。多加一些头条互粉、互评的群等等，让文章发出去后尽快有人点击查看。</p><p><strong>百度系列：</strong></p><p><a href=\"https://www.tn721.cn/go?url=https://lusongsong.com/reed/10896.html\" target=\"_blank\" rel=\"noreferrer noopener\">百度体系是很大的一个引流渠道</a>，这里我主要提一点贴吧的引流技巧，因为贴吧这块需要配合软件，IP等等，所以我这里只写一些小细节的东西。</p><p>关于贴吧这一块，如果不配合软件的话。我们要做的其实就三块：</p><p>第一修内功，你的软文一定要足够有吸引力;</p><p>第二铺渠道，你的涉及面一定要足够大。</p><p>第三执行力，我之前提过贴吧引流的核心就在一个“顶”字。所以你发的东西有没有再回帖，有没有再持续更新，是你贴吧能否引来流量的核心。</p><p>而且在顶的过程中，要大家一起相互配合，可以放到群里让大家帮你回复下。然后就是为什么你的帖子发了之后被删了，封号了。这里就跟你发的内容有关了，比如是不是有违规词、广告、不符合贴吧规则等等。</p><p>所以做贴吧引流前，第一个要看的就是贴吧的规则，把贴吧规则看完之后我们要开始研究如何避免被删，比如说大小号配合，我先用一个账号，头像、昵称等设置为联系方式【注意用谐音或者v3形式】然后发一些资料，案例，联系方式等。再用大号去发软文，大号发完软文后@小号。这样就避免了被删的可能，还有就是，在一个没有吧主的贴吧，发一篇广告文，然后把文章链接放到另一个贴吧的帖子下。还有就是要每天清空一下你的cookie。一般在浏览器设置里的上网痕迹里。同时要电脑、手机配合发帖，同时手机去发语音贴等等。</p><p>作者/公众号：超脑内网</p><br/><br/>', 0, 2, 'https://images.lusongsong.com/zb_users/upload/2019/09/201909063673_906.jpg', 1, 25, 1571733163);
INSERT INTO `tn_post` VALUES (19, '微信号涨粉互推技巧', '<p><br/></p><h3>一、关于微信互推</h3><p>关于微信互推，相信无论是做互联网行业的朋友，还是做传统行业的朋友，应该都见过或者实践过。</p><p>对于微信号涨粉以及后续的粉丝流量变现，微信互推都称得上是一种很好的营销推广手段，尤其是互联网流量日益匮乏，流量获取成本不断提升的今天。</p><p>在微信或微信公众号的初期，很多互联网人推广简单直白的微信或者微信公众号互推，只需要随便甩出一段文案+一张微信二维码即可轻松互推涨粉，那时候大家对这种模式感觉还很新颖，也有兴趣去尝试，所以效果很好。</p><p>但是现在因为大家对这种套路习以为常了，而且见的多了也产生了抵触心理，所以效果不如从前，那么微信互推就真的不能做了吗?</p><p>当然不是。</p><p>和其他互联网项目一样，其实并不是说不能做，而是要更加专业化或者精细化运营，如果再向过去那样简单粗暴没有章法的去做注定是要淘汰出局了。</p><p>那么现在如何进行高效率的微信互推呢?</p><h3>二、互推的注意事项</h3><p><strong>(1)首先我举个例子：</strong></p><p>A是个卖运动鞋的，货源是福建莆田。B是服装的，货源是杭州四季青。两个人进行了一个精明的合作，因为运动鞋和服装的客户群体是相同的，如果买了衣服，是不是可以带双鞋呢?</p><p>AB开始了朋友圈互推，而这种互推往往是一个双赢的结果，他们不存在竞争。</p><p>那么假如A是卖老年人养生产品的，B是卖教育用品的，若双方互推，那你会购买吗?我肯定不会选择，因为这两行的差异极大，客户群体也完全不同。</p><p>所以说，互推并不是随便找个人互推，至少应该是客户群体重叠才可以，否则只会适得其反。</p><p><strong>(2)互推的形式注意是一句话文案+一张微信二维码。</strong></p><p>文案尽量控制在50字以内，不能折叠，内容一般是个人介绍+利益诱饵(比如推荐一位朋友XXX，取得什么成就，现在加他微信回复XX即可免费赠送XX课程)，图片只需要配一张二维码图片即可，也可以多加几张，一般不超过3张，二维码只要一张。</p><p><strong>(3)互推后要监督互推的效果，</strong>看看涨粉情况，通过测试效果来决定后续是否继续合作。</p><p><strong>(4)不要越级</strong></p><p>如果你有1万的粉丝，你想与10万的粉丝进行互推，那肯定没人愿意做的，但你有10万粉，去找了一个2万多的号，我相信你也不愿意。所以最好的办法就是找门当户对，实力相当的人去推。</p><p><strong>(5)不要和不讲信誉的人互推，影响声誉。</strong></p><p><strong>(6)坚决不要推做不正当生意的人互推。</strong></p><p><strong>(7)推荐人，不要推产品，</strong>因为推产品很容易引起读者反感，搞不好会让你掉粉。</p><h3>三、互推合作</h3><p><strong>互推合作对象主要是4类人：</strong></p><p>1.亲朋好友</p><p>2.客户</p><p>3.同行</p><p>4.跟自己行业客户群体重叠的人</p><p><strong>互推的时间：</strong></p><p>双方最好商量一个确定的时间统一推送。初期不建议大家找很多人互推，频繁互推会使读者使用体验很差，效果只会适得其反甚至掉粉，在节假日或者周末的时候互推是不错的选择。</p><p>从一天的时间段来讲的话，建议是选择这三个时间段：</p><p>早上8:00~9:00</p><p>中午1:00~2:00</p><p>下午4:00~5:00</p><h3>四、互推的形式</h3><p>关于互推，主要互推形式主要是以下四种：</p><p>1.朋友圈图文</p><p>2.公众号文章</p><p>3.公众号转载</p><p>4.公众号菜单栏</p><p>其中微信朋友圈推广毫无疑问是效果最好的，毕竟可以直接在微信里互动，其他几种形式也有，这个要看具体的需求，如果是为了公众号涨粉或者文章阅读量自然就是在公众号里比较合适。</p><h3>五、关于付费推广</h3><p>目前很多交易平台或者互联网圈子里已经有几万粉丝的大咖都推出了付费推广的业务，关于付费推广其实也是一种不错的选择，如果你想快速涨粉而且预算充足的情况下可以选择付费互推，这个就类似于资本为了快速占领市场前期选择烧钱一个道理，效果也是立竿见影的。</p><p>作者：步鲸云 公众号：引流侠步鲸云</p><br/><br/>', 0, 2, '', 1, 16, 1571733213);
INSERT INTO `tn_post` VALUES (20, '心得分享：互联网新手需要注意的事情 ', '<p><br/></p><p>我，草根出身，初中学历，缺的是钱，不缺热情!</p><p>和大部分人一样，接触互联网都是因为网游，而不是因为可以赚钱。</p><p>2010年开始泡吧，那时本地的网吧清一色的DNF、梦幻西游游戏界面。</p><p>几乎所有人进入网吧的目的都是一样的，就是为了组团游戏，我也一样，痴迷于腾讯的“DNF”游戏</p><p>苦于练级难，又想走捷径，就在网上到处搜索“DNF外挂”，找来的外挂更新速度很慢，每次还没进副本，就被腾讯检测封号N小时!</p><p>抱怨游戏外挂作者为什么那么笨，怎么不能及时更新!</p><p>为了解决这个问题，接触到了中文编程神器“易语言”，一个多月时间，从零开始学习“易语言”，做出了属于自己的第一款游戏外挂。</p><h3>制作游戏外挂给我带来第一桶金</h3><p>体验到游戏外挂带来的快感，渐渐地却对游戏丧失了兴趣，却发现同行都在用外挂收费来赚钱!</p><p>通过游戏外挂收费，第一次感受到互联网的魅力，从前玩游戏费钱，现在还可以赚钱!</p><p>为了更好的宣传自己的外挂，学习搭建了外挂的官网，也正是这次转机让我了解到“站长”这个神秘的职业。</p><p>随着外挂的高频更新，泉水般涌出的同行，我疲惫不堪，决定放弃外挂制作的行业，转身做“站长”!</p><p>2012年依次建设了属于自己的下载站、电影站、情感论坛，这些网站通过挂广告收取广告费，都赚到了钱。</p><h3>第一个项目，我以失败告终</h3><p>2014年正式踏入网赚圈，操作分类信息项目(通过58、百姓网等分类信息网站卖手机)</p><p>QQ列表一名好友，每天都在发关于成交的截图，打动了我，交了学费，上了车!</p><p>梦想着有朝一日也能像他一样，日入上千。</p><p>项目操作了有两个星期，却没有通过分类信息网站引来任何客户，当地没有人使用这些平台，自然没有流量。无奈，宣告放弃!</p><h3>2015年夏天，我日入上千!</h3><p>一次巧合认识了当时带我入行的大佬，详细的告诉了我项目的操作流程和方法，为之付出的是3000大洋，从付费那天起，就傻傻的执行!</p><p>准备了一周时间，开始变现的当晚，收款500+，我暗自窃喜，咸鱼翻身的机会来了!</p><p>凌晨四点，难掩激动的心情，跑到楼下超市买了一桶泡面，两根火腿肠，四个鸡蛋，我要狠狠地犒劳自己!</p><p>日复一日，看着每天不断增加的余额，我膨胀了，花钱大手大脚起来!</p><p>随着政策的改变，日入上千的日子仅仅持续了三个月，收益陡然下降，想起熬夜的疲惫，不如给自己放个假。</p><p>在2016年初，靠着这个项目的收入和存款提了人生第一台车!</p><h3>再次化身“站长”，陪你一起成长!</h3><p>近几年随着互联网的发展，越来越多的小白听说某某通过网络赚了很多钱，就盲目的加入这个行业，换来的却是无情的收割!</p><p>曾经的网赚圈是多么的单纯，流传的网赚项目是多么的靠谱，只要上手操作就能赚到钱!那个时候，“韭菜”只是用来包饺子的!</p><p>通过这几年的铺垫，有幸能混入多个知名的互联网圈子，圈子内的大佬都很和蔼，平时除了插科打诨，还会分享一些网赚项目，从而使我能拥有一手的信息，一手的资源。</p><p>再次建站，我有了不一样的视角，也看了许多同类型网站，发现大部分标注“网赚”的网站并没有提供给我想要的东西，更别提那些有价值的网赚项目。</p><p>所以我决定，亲自上阵，把自己手中的靠谱网赚项目，这多年来实践经验和一些感悟，总结并分享给大家。</p><h3>送给所有网赚新手的忠告</h3><p>这些年经历了很多网赚项目，也错过了很多机遇。看到了身边的人因为互联网而飞黄腾达，也看到了好朋友因为网赌而万劫不复。这篇文章用来警示自己，也用来帮助你!便于理解，我将系统化的总结，作为一个网赚新手，你应该做什么，怎么做，为你提供高效的学习框架和寻找网赚项目的方法：</p><h3>明确自身目标：选择适合自己的项目</h3><p>做网赚就是为了赚钱，虽然庸俗，但在新手眼里，总觉得高不可攀，其实不然，目前互联网流传的网赚项目主要分为以下几种类型：</p><p><strong>短期项目</strong></p><p>俗称“短平快”项目，如：某平台邀请注册奖励，某平台活动送现金。诸如此类，指的是有时效性的网赚项目。</p><p>优点：见钱速度快，操作流程简单</p><p>缺点：可持续性差</p><p><strong>长期项目</strong></p><p>所谓的长期项目也只是相对于“短期项目”而言，多是按照平台的规则框架操作，如：淘宝店群，某自媒体平台撸收益，某平台引流变现卖产品。</p><p>优点：可持续性强，遵守平台规则可以长期操作</p><p>缺点：见钱速度相对就慢一些，流程也会更繁琐</p><p>引流卖粉</p><p>引流卖粉是近几年比较热的行业，这些人服务于网赚圈，为网赚项目提供流量，众所周知的减肥粉、宝妈粉、金融粉一度涨到100元-150元/个</p><p>优点：见钱速度快，现结</p><p>缺点：容易封号，投入人力大，资源消耗快</p><p>目前网赚项目通常寄生在平台规则之下，平台规则一旦完善，现有玩法就会失效，收入水平也会大幅度下降，需要及时作出改变。明白了各种网赚项目的特点，再来为大家拓展一个思路：优先选择有利于资源累计的网赚项目。</p><p>很多网赚项目都是一些流氓项目，虽然说当时赚到钱了，可是当你停止操作回头看的时候，发现除了钱什么都没有留下。</p><p>流量是互联网项目的根本，有了流量就有钱，所以在选择项目的时候，要考虑这个项目是否能为我带来后续的资源累计。当然，你只考虑赚钱也没有错!</p><p>可累计的资源：好友、粉丝、人脉、平台帐号、经验，包含可以循环利用的资源：如手机号卡、SFZ信息等</p><h3>不要急于求成：小钱看不上，大钱赚不到</h3><p>相信“冰冻三尺非一日之寒”，知识、财富亦是如此，都需要通过点点滴滴的累计换取。在选择网赚项目前，新手往往容易被无良标题党诱惑：</p><p>1-1、月入3万，只需要看了本篇.....</p><p>1-2、日入2000，某某网赚项目，赶快加入</p><p>1-3、一天提现1000元，这个项目强烈推荐</p><p>这种标题，是不是更能点燃你的激情，先不考虑项目真实性问题，作为一个新手，要能认识到自己“不行”，多做和自己能力相符的事，网赚就要一步一个脚印，由小变大，从一天挣10元，到一天挣1000元，是需要一个漫长的过程，而不是一蹴而就。先从赚小钱开始，慢慢的累计你的能力和经验。</p><p>2-1、月入5000，XX项目分享</p><p>2-2、日入100，三十天成就自己</p><p>2-3、一天提现120，学习使人进步</p><p>日入100不见得是小，日入5000不见得是真!</p><h3>保持时刻学习：投资自己才是最有意义的事。</h3><p>合理的规划时间，每天除了操作项目之外，也要给自己学习的时间。</p><p>学习当然是为了更好的成长，用来赚更多的钱，即将踏入网赚圈的你，需要掌握更多的技能来帮助自己!</p><p>推荐几个学习的方向：</p><p><strong>1、PS</strong></p><p>为了作图不求人，这个是网赚人的必备技能</p><p><strong>2、网站搭建</strong></p><p>如百度竞价、跳转二维码，都需要网站搭建的基本功，为了避免自己“FTP”都不知道是什么，就要努力学习!</p><p><strong>3、脚本制作</strong></p><p>目前引流界的大佬，除了招聘员工来完成引流工作以外，还通过一些自动化脚本来完成，本博客未来为大家提供引流脚本制作教程!</p><p><strong>4、前端设计</strong></p><p>搭建一个属于自己的网站，需要漂亮的界面，离不开前端技术的支持，如果有时间学习这个，还是很有必要的!</p><p><strong>5、PHP编程</strong></p><p>PHP作为当前主流网站编程代码语言，强烈推荐想要学习网站编程的朋友从PHP开始入手</p><p><strong>6、视频剪辑</strong></p><p>2019年，绝对是小视频流量爆发的一年，创作优秀的视频作品，虽然很多APP支持剪辑功能，但我认为还是能使用PR类软件，独立完成剪辑更为炫酷!</p><p><strong>7、文字撰写</strong></p><p>只要你会说话，会表达自己，把你的想法写出来，那这就是一篇好的文章，首先你要学会总结!</p><h3>拥有谦逊的心：低调，才是最牛逼的炫耀</h3><p>互联网中不乏有很多能赚快钱的机会，当你有幸操作时，请一定要记得：低调!低调!低调!</p><p>当你有朝一日身为大佬的时候，也要保持谦逊的态度，那高高在上的样子实在让人讨厌!</p><p>曾经看过一篇文章“毁掉一个人最好的方式，就是让他赚快钱”感同身受，继而在这里，一定要写出来告诉你们!</p><p>赚了钱，要合理的规划，用来投资自己、增添幸福、回报家人。</p><p>别忘了要感谢一直默默支持你，陪在你身边的那个人!</p><h3>应该学会付出：学会为价值买单</h3><p>见好友在QQ空间求“优酷会员”“爱奇艺会员”，这些网络特权的收费不算贵，不过一张电影票的钱，可以换来一个月的特权，大家也不是买不起，只是不愿意为价值付费!</p><p>微信群内大佬分享了网赚项目操作经验，大家纷纷鼓掌叫好，并私发了红包，虽然不多。但这就是一个态度，一个愿意为价值买单的态度，对自己有所帮助，就应该学会付出!</p><p>免费的就是最贵的，用人情、资源换来的免费，不如痛痛快快的明码标价!</p><p>希望以上所有内容，对你有所启发，有所帮助!</p><p>作者：旭旭 QQ：85078997</p><br/><br/>', 0, 3, '', 1, 49, 1571733242);

-- ----------------------------
-- Table structure for tn_post_collection
-- ----------------------------
DROP TABLE IF EXISTS `tn_post_collection`;
CREATE TABLE `tn_post_collection`  (
  `uid` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_post_collection
-- ----------------------------
INSERT INTO `tn_post_collection` VALUES (48, 35);
INSERT INTO `tn_post_collection` VALUES (48, 36);

-- ----------------------------
-- Table structure for tn_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_rule`;
CREATE TABLE `tn_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(2) NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu` int(2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_rule
-- ----------------------------
INSERT INTO `tn_rule` VALUES (1, 0, '&#xe66f;', '', '会员管理', 0);
INSERT INTO `tn_rule` VALUES (2, 0, '&#xe655;', '', '内容管理', 0);
INSERT INTO `tn_rule` VALUES (3, 0, '&#xe673;', '', '权限管理', 0);
INSERT INTO `tn_rule` VALUES (4, 0, '&#xe716;', '', '系统设置', 0);
INSERT INTO `tn_rule` VALUES (5, 0, '&#xe656;', '', '主题插件', 0);
INSERT INTO `tn_rule` VALUES (6, 0, '&#xe64c;', '', '链接管理', 0);
INSERT INTO `tn_rule` VALUES (7, 0, NULL, '', '【前端】用户权限（以下权限为用户权限，不建议更改）', 1);
INSERT INTO `tn_rule` VALUES (8, 0, NULL, '', '其他权限', 1);
INSERT INTO `tn_rule` VALUES (9, 4, NULL, 'admin/system/customCode', '自定义代码', 0);
INSERT INTO `tn_rule` VALUES (10, 4, NULL, 'admin/system/Basics', '基本设置', 0);
INSERT INTO `tn_rule` VALUES (11, 1, NULL, 'admin/user/delGroup', '删除用户组', 1);
INSERT INTO `tn_rule` VALUES (12, 1, NULL, 'admin/user/addRole', '添加角色', 1);
INSERT INTO `tn_rule` VALUES (13, 1, NULL, 'admin/user/groupRule', '用户组权限详情', 1);
INSERT INTO `tn_rule` VALUES (14, 2, NULL, 'admin/post/postList', '文章列表', 0);
INSERT INTO `tn_rule` VALUES (15, 1, NULL, 'admin/user/userList', '会员列表', 0);
INSERT INTO `tn_rule` VALUES (16, 1, NULL, 'admin/user/saveRule', '修改权限', 1);
INSERT INTO `tn_rule` VALUES (17, 1, NULL, 'admin/user/userAdd', '添加用户', 1);
INSERT INTO `tn_rule` VALUES (18, 1, NULL, 'admin/user/delUser', '删除用户', 1);
INSERT INTO `tn_rule` VALUES (19, 2, NULL, 'admin/post/postAdd', '发布文章', 0);
INSERT INTO `tn_rule` VALUES (20, 2, NULL, 'admin/index/upload', '上传图片', 1);
INSERT INTO `tn_rule` VALUES (21, 1, NULL, 'admin/user/userStatus', '修改用户状态', 1);
INSERT INTO `tn_rule` VALUES (22, 8, NULL, 'admin/index/welcome', '用户欢迎页', 1);
INSERT INTO `tn_rule` VALUES (23, 2, NULL, 'admin/post/category', '文章分类', 0);
INSERT INTO `tn_rule` VALUES (24, 2, NULL, 'admin/post/delPost', '删除文章', 1);
INSERT INTO `tn_rule` VALUES (25, 4, NULL, 'admin/system/topArticle', '修改置顶推荐', 1);
INSERT INTO `tn_rule` VALUES (26, 4, NULL, 'admin/system/carousel', '修改轮播图配置', 1);
INSERT INTO `tn_rule` VALUES (27, 43, NULL, 'admin/system/editLink', '修改友情链接', 1);
INSERT INTO `tn_rule` VALUES (28, 43, NULL, 'admin/system/addLink', '添加友情链接', 1);
INSERT INTO `tn_rule` VALUES (29, 43, NULL, 'admin/system/deleteLink', '删除友情链接', 1);
INSERT INTO `tn_rule` VALUES (30, 4, NULL, 'admin/system/deleteNav', '删除导航', 1);
INSERT INTO `tn_rule` VALUES (31, 4, NULL, 'admin/system/saveNav', '修改导航', 1);
INSERT INTO `tn_rule` VALUES (32, 3, NULL, 'admin/user/adminRole', '角色管理', 0);
INSERT INTO `tn_rule` VALUES (33, 7, NULL, 'home/member/index', '个人中心', 1);
INSERT INTO `tn_rule` VALUES (34, 7, NULL, 'home/post/postAdd', '投稿文章', 1);
INSERT INTO `tn_rule` VALUES (35, 7, NULL, 'home/post/delPost', '删除文章', 1);
INSERT INTO `tn_rule` VALUES (36, 7, NULL, 'home/post/cancelCollection', '取消收藏文章', 1);
INSERT INTO `tn_rule` VALUES (37, 7, NULL, 'home/member/updateUser', '修改资料', 1);
INSERT INTO `tn_rule` VALUES (38, 7, NULL, 'home/member/upload', '修改头像', 1);
INSERT INTO `tn_rule` VALUES (39, 7, NULL, 'home/member/rePassword', '修改密码', 1);
INSERT INTO `tn_rule` VALUES (40, 5, NULL, 'admin/theme/index', '主题管理', 0);
INSERT INTO `tn_rule` VALUES (41, 6, NULL, 'admin/link/index', '链接列表', 0);
INSERT INTO `tn_rule` VALUES (42, 4, NULL, 'admin/system/smtp', '邮箱配置', 0);
INSERT INTO `tn_rule` VALUES (43, 4, NULL, 'admin/system/site', '网站SEO', 0);
INSERT INTO `tn_rule` VALUES (44, 4, NULL, 'admin/system/addNav', '添加友情链接', 1);

-- ----------------------------
-- Table structure for tn_system
-- ----------------------------
DROP TABLE IF EXISTS `tn_system`;
CREATE TABLE `tn_system`  (
  `config` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `extend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`config`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_system
-- ----------------------------
INSERT INTO `tn_system` VALUES ('carousel', '14,18', NULL, NULL);
INSERT INTO `tn_system` VALUES ('siteInfo', '{\"title\":\"TnCMS\",\"subtitle\":\"\\u8f7b\\u3001\\u7b80\\u3001\\u5feb\\u5185\\u5bb9\\u7ba1\\u7406\\u7cfb\\u7edf\",\"keyword\":\"tncms,\\u8f7bcms,\\u5929\\u5e74\\u7f51\\u7edc,thinkphp6\",\"description\":\"\\u8f7b\\u3001\\u7b80\\u3001\\u5feb\\u5185\\u5bb9\\u7ba1\\u7406\\u7cfb\\u7edf\",\"copyright\":\"Copyright \\u00a9 2019 TnCMS.\\u4fdd\\u7559\\u6240\\u6709\\u6743\\u5229  \\u6ec7ICP\\u590717000713\\u53f7\"}', NULL, NULL);
INSERT INTO `tn_system` VALUES ('smtp', '', '', '');
INSERT INTO `tn_system` VALUES ('statistics', '', NULL, NULL);
INSERT INTO `tn_system` VALUES ('theme', 'default', 'amaze', NULL);
INSERT INTO `tn_system` VALUES ('topArticle', ' 17,18,19,20', NULL, NULL);

-- ----------------------------
-- Table structure for tn_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_user`;
CREATE TABLE `tn_user`  (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT 0,
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `integral` int(255) NULL DEFAULT 0,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_user
-- ----------------------------
INSERT INTO `tn_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@admin.com', 1, NULL, NULL, 0, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
