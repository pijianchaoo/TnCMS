# TnCMS内容管理系统

## v1.1.0版本更新明

1.升级ThinkPHP框架为ThinkPHP6正式版本

2.修改优化前端界面，采用element框架重写界面

3.增加主题修改功能，后台可修改手机端，PC端对应主题

4.新增手机端主题

5.精简URL地址，便于搜索引擎收录。

6.修复已知BUG

7.更多功能敬请期待，发现BUG或者有更好的建议，欢迎提出。

## 介绍
TnCMS原创轻内容管理系统，基于ThinkPHP6+layUI开发。

## 软件架构

前端：LayUI

后端：ThinkPHP6


## 功能特点

1. 权限管理

2. 用户管理

3. 文章管理

4. 微信公众号登陆

5. 前端用户中心

6. 用户投稿公众号推送通知

7. 敬请期待


## 微信登录配置说明

1.微信公众号里配置服务器地址为：https://域名/home/wechat/wxlogin。

2.修改 home\controller\Wechat.php 文件下的参数为公众号配置：


```

    protected $TOKEN = "";
    protected $APPID = "";
    protected $SECRET = "";

```


#### 演示

后台默认账号：admin

密码:123456

注意：网站的根目录要设置为public目录

演示地址：[点击访问](http://v6.tn721.cn)

官网：[点击访问](http://www.tn721.cn)

#### 反馈建议

意见反馈请关注下方公众号，直接留言问题，看到后会及时回复，感谢支持。


![输入图片说明](https://images.gitee.com/uploads/images/2019/1009/234545_1f18373e_1836397.jpeg "天年网络公众号.jpg")